#!/usr/bin/env python
# coding: utf-8

# # Models
# 
# ### The models are evaluated by ROC and execution time on 6 benchmark datasets. All datasets are split (70% for training and 30% for testing). 
# 
# The model covered includes:
#   1. Outlier Ensembles and Combination Frameworks
#      1. **Isolation Forest**
#      2. **Feature Bagging**
#      3. **LODA**
#      4. **LSCP**
#      5. **AutoEncoder**
#      6. **Autoencoder Ensemble**
# 

# In[1]:


from __future__ import division
from __future__ import print_function
import os
import sys
from time import time
import pickle

sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname("__file__"), '..')))
import warnings

warnings.filterwarnings("ignore")

import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.model_selection import train_test_split
from scipy.io import loadmat
from pyod.models.abod import ABOD
from pyod.models.cblof import CBLOF
from pyod.models.feature_bagging import FeatureBagging
from pyod.models.hbos import HBOS
from pyod.models.iforest import IForest
from pyod.models.knn import KNN
from pyod.models.lof import LOF
from pyod.models.mcd import MCD
from pyod.models.ocsvm import OCSVM
from pyod.models.pca import PCA
from pyod.models.lscp import LSCP
from pyod.models.loda import LODA
from pyod.models.auto_encoder import AutoEncoder

from pyod.utils.utility import standardizer
from pyod.utils.utility import precision_n_scores

from typing import Tuple
from tqdm import tqdm
from datetime import datetime
from sklearn.preprocessing import StandardScaler
from sklearn.base import TransformerMixin
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.metrics import (
    classification_report,
    precision_recall_curve,
    cohen_kappa_score,
    roc_auc_score,
)
from tensorflow import keras
from tensorflow.keras import layers



# In[2]:


class RandAE(tf.keras.Sequential):
    def __init__(self, input_dim, hidden_dims, drop_ratio=0.5, **kwargs):
        super(RandAE, self).__init__(**kwargs)

        self.input_dim = input_dim
        self.hidden_dims = hidden_dims
        self.drop_ratio = drop_ratio

        self.layer_masks = dict()

        self.build_model()

    def build_model(self) -> None:
        """
        Adds the layers and records masks.
        """

        self.add(layers.InputLayer(self.input_dim, name="input"))

        for i, dim in enumerate(self.hidden_dims):
            layer_name = f"hidden_{i}"
            layer = layers.Dense(
                dim, activation="relu" if i > 0 else "sigmoid", name=layer_name
            )
            self.add(layer)

            # add layer mask
            self.layer_masks[layer_name] = self.get_mask(layer)

        layer_name = "output"
        output_layer = layers.Dense(
            self.input_dim, activation="sigmoid", name=layer_name
        )
        self.add(output_layer)
        self.layer_masks[layer_name] = self.get_mask(output_layer)

    def get_mask(self, layer, mode="replacement") -> np.ndarray:
        """
        Build mask for a layer.
        """

        shape = layer.input_shape[1], layer.output_shape[1]

        if mode == "by_ratio":
            mask = np.random.choice(
                [0.0, 1.0], size=shape, p=[self.drop_ratio, 1 - self.drop_ratio]
            )
        elif mode == "replacement":
            mask = np.ones(shape=(shape[0] * shape[1],))
            zero_idx = np.random.choice(mask.shape[0], size=mask.shape[0], replace=True)
            zero_idx = np.unique(zero_idx)
            mask[zero_idx] = 0
            mask = mask.reshape(shape)
        else:
            raise NotImplementedError(
                f"Mode {mode} not implemented, choose from 'replacement' (original implementation) or 'by_ratio'."
            )

        return mask

    def load_masks(self, mask_pickle_path) -> None:


        with open(mask_pickle_path, "rb") as handle:
            self.layer_masks = pickle.load(handle)

    def get_encoder(self) -> keras.Sequential:
        #Get the encoder from the full model.

        n_layers = (len(self.hidden_dims) + 1) // 2
        encoder_layers = [layers.Input(self.input_dim)] + self.layers[:n_layers]

        return keras.Sequential(encoder_layers)

    def mask_weights(self) -> None:
        #Apply the masks to each layer in the encoder and decoder.

        for layer in self.layers:
            layer_name = layer.name
            if layer_name in self.layer_masks:
                masked_w = layer.weights[0].numpy() * self.layer_masks[layer_name]
                b = layer.weights[1].numpy()
                layer.set_weights((masked_w, b))

    def call(self, data, training=True) -> tf.Tensor:
        # mask the weights before original forward pass
        
        self.mask_weights()

        return super().call(data)

class BatchAdaptiveDataGenerator(keras.utils.Sequence):
    def __init__(
        self,
        x,
        start_batch_size,
        epochs,
        subsample=0.3,
        start_data_ratio=0.5,
        shuffle=True,
        verbose=False,
    ):
        self.x = x
        self.subsample = subsample
        self.verbose = verbose

        if self.subsample:
            sample_idx = np.random.choice(
                self.x.shape[0], size=int(self.subsample * self.x.shape[0])
            )
            self.x = self.x[sample_idx]

        # initial training params
        self.epochs = epochs
        self.start_batch_size = start_batch_size
        self.start_data_ratio = start_data_ratio
        self.steps_per_epoch = int(
            self.start_data_ratio * self.x.shape[0] / self.start_batch_size
        )

        # adaptive learning param to increase batch_size after each epoch
        self.alpha = np.exp(np.log(1 / self.start_data_ratio) / self.epochs)
        self.shuffle = shuffle

        # per epoch variables
        self.epoch = 0
        self.current_x = None
        self.current_batch_size = self.start_batch_size
        self.on_epoch_end()

    def __len__(self):
        return self.steps_per_epoch

    def __getitem__(self, idx):

        batch = self.current_x[
            idx * self.current_batch_size : (idx + 1) * self.current_batch_size
        ]

        return batch, batch

    def on_epoch_end(self):

        # update training data by slicing and shuffling
        current_x_size = int(self.current_batch_size * self.steps_per_epoch)
        self.current_x = self.x[:current_x_size]

        # shuffle rows to mix data in different batches
        if self.shuffle:
            rand_idx = np.arange(self.current_x.shape[0])
            np.random.shuffle(rand_idx)
            self.current_x = self.current_x[rand_idx]

        if self.verbose:
            print(
                f"Epoch {self.epoch + 1} -- {self.current_x.shape[0] / self.x.shape[0] * 100}% data"
            )

        # update batch size
        self.current_batch_size = int(self.start_batch_size * self.alpha ** self.epoch)
        self.epoch += 1
    
def eval_ensemble(ensemble, input_data, input_labels, contamination):

    results = dict()

    # make prediction by each ensemble component
    predictions = [model.predict(input_data) for model in ensemble]

    # SSE based on reconstruction for each component
    reconstruction_loss = np.stack(
        [np.square(pred - input_data).sum(axis=1) for pred in predictions], axis=1
    )

    results["reconstruction_loss"] = reconstruction_loss

    # scale the std to account for different levels of overfitting
    scaler = StandardScaler(with_mean=False)
    reconstruction_loss = scaler.fit_transform(reconstruction_loss)

    # find the median loss for each sample
    median_loss = np.median(reconstruction_loss, axis=1)


    # min-max scaling the reconstruction loss
    min_ = median_loss.min()
    max_ = median_loss.max()
    scaled_loss = (median_loss - min_) / (max_ - min_)
    
    roc = round(roc_auc_score(input_labels, scaled_loss), ndigits=4)
    
    results["auc"] = roc
    return results


# In[3]:


mat_file_list = ['vertebral.mat',
                 'ionosphere.mat',
                 'arrhythmia.mat',
                 'mnist.mat',
                 'satellite.mat',
                 'musk.mat']

random_state = np.random.RandomState(792)

#df_columns = ['Data', '#Samples', '# Dimensions', 'Outlier Perc','ABOD', 'CBLOF', 'FB', 'HBOS', 'IForest', 'KNN','Average KNN', 'LOF', 'MCD','LODA','LSCP','OCSVM', 'PCA','AutoEncoder']
df_columns = ['Data', '#Samples', '# Dimensions', 'Outlier Perc', 'FB', 'IForest','LODA','LSCP','AutoEncoder','EnsembleAE']
roc_df = pd.DataFrame(columns=df_columns)
prn_df = pd.DataFrame(columns=df_columns)
time_df = pd.DataFrame(columns=df_columns)
batch_size = [5, 8, 8, 8, 32, 32]
reconstrction_error_list = []
All_score_list = []
number = 0


for mat_file in mat_file_list:
    print("\n... Processing", mat_file, '...')
    mat = loadmat(os.path.join('data', mat_file))

    X = mat['X']
    y = mat['y'].ravel()
    outliers_fraction = np.count_nonzero(y) / len(y)
    outliers_percentage = round(outliers_fraction * 100, ndigits=4)
    
    roc_list = [mat_file[:-4], X.shape[0], X.shape[1], outliers_percentage]
    time_list = [mat_file[:-4], X.shape[0], X.shape[1], outliers_percentage]

    # 70% data for training and 30% for testing
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3,
                                                        random_state=random_state)

    X_train_norm, X_test_norm = standardizer(X_train, X_test)
    

    classifiers = {
        'Feature Bagging': FeatureBagging(contamination=outliers_fraction,
                                          random_state=random_state),
        'Isolation Forest': IForest(contamination=outliers_fraction,
                                    random_state=random_state),
        'LODA': LODA(contamination=outliers_fraction),
        'LSCP':LSCP([LOF(n_neighbors=15), LOF(n_neighbors=20),
                     LOF(n_neighbors=25), LOF(n_neighbors=35)],contamination=outliers_fraction, random_state=random_state),          
        'AutoEncoder': AutoEncoder(hidden_neurons=[8,4,4,8],
            contamination=outliers_fraction, random_state=random_state),    
    }   
    for clf_name, clf in classifiers.items():
        t0 = time()
        clf.fit(X_train_norm)
        test_scores = clf.decision_function(X_test_norm)
        t1 = time()
        duration = round(t1 - t0, ndigits=4)
        time_list.append(duration)

        roc = round(roc_auc_score(y_test, test_scores), ndigits=4)
        # prn = round(precision_n_scores(y_test, test_scores), ndigits=4)

        print('{clf_name} ROC:{roc}, '
              'execution time: {duration}s'.format(
            clf_name=clf_name, roc=roc, duration=duration))
        roc_list.append(roc)

    ensemble = []     
    N_MODELS = 25 
    t0 = time()
    
    for i in tqdm(range(N_MODELS)):
        model = RandAE(**{
            "input_dim": mat['X'].shape[1],
            "hidden_dims": [64, 32, 32, 64],  
            "drop_ratio": 0.2,
        })
        model.compile(**{
            "optimizer": "adam",
            "loss": "binary_crossentropy",  
            "run_eagerly": True,  
        })
        data_generator = BatchAdaptiveDataGenerator(X_train, **{"start_batch_size": batch_size[number],
                                                                    "epochs": 200,
                                                                    "subsample": 0.1,
                                                                    })
        model.fit(data_generator, **{"epochs": 200, "verbose": 1, "workers": -1})
        
        model.save_weights(f"models/randae_model_{i}")
        with open(f'model_{i}_masks.pickle', 'wb') as handle:
            pickle.dump(model.layer_masks, handle, protocol=pickle.HIGHEST_PROTOCOL)
        ensemble.append(model)
    number += 1
    eval_results = eval_ensemble(ensemble, X_test_norm, y_test, contamination=outliers_fraction)
    t1 = time()
    duration = round(t1 - t0, ndigits=4)
    time_list.append(duration)
    roc_list.append(eval_results['auc'])
    reconstrction_error_list.append(eval_results["reconstruction_loss"])
    temp_df = pd.DataFrame(time_list).transpose()
    temp_df.columns = df_columns
    time_df = pd.concat([time_df, temp_df], axis=0)

    temp_df = pd.DataFrame(roc_list).transpose()
    temp_df.columns = df_columns
    roc_df = pd.concat([roc_df, temp_df], axis=0)


# In[4]:


print('Time complexity')
time_df


# Analyze the performance of ROC

# In[5]:


print('ROC Performance')
roc_df


# In[27]:


import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.manifold import TSNE
get_ipython().run_line_magic('matplotlib', 'inline')
number = 0
threshold_list = [4, 4, 6, 4, 6, 5]
for mat_file in mat_file_list:
    print("\n... Processing", mat_file, '...')
    mat = loadmat(os.path.join('data', mat_file))

    X = mat['X']
    y = mat['y'].ravel()
    outliers_fraction = np.count_nonzero(y) / len(y)
    outliers_percentage = round(outliers_fraction * 100, ndigits=4)
    
    roc_list = [mat_file[:-4], X.shape[0], X.shape[1], outliers_percentage]
    time_list = [mat_file[:-4], X.shape[0], X.shape[1], outliers_percentage]

    # 70% data for training and 30% for testing
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3,
                                                        random_state=random_state)

    X_train_norm, X_test_norm = standardizer(X_train, X_test)

    reconstruction_loss = reconstrction_error_list[number]

    scaler = StandardScaler(with_mean=False)
    reconstruction_loss = scaler.fit_transform(reconstruction_loss)

    median_loss = np.median(reconstruction_loss, axis=1)

    min_ = median_loss.min()
    max_ = median_loss.max()
    scaled_loss = (median_loss - min_) / (max_ - min_)
    

    threshold = threshold_list[number]
    bigloss_idx = np.where(scaled_loss > threshold)[0]
    smallloss_idx = np.random.choice(np.where(scaled_loss <= threshold)[0], size=(2500,))
    idx = np.concatenate([bigloss_idx, smallloss_idx], axis=0)
    plot_X = X_test[idx]
    plot_y = y_test[idx].ravel()
    plot_loss = scaled_loss[idx]
    tsne = TSNE(n_components=2)
    tsne_data = tsne.fit_transform(plot_X)
    plt.figure(figsize=(15, 6))
    plt.subplot(1, 2, 1)
    plt.scatter(tsne_data[plot_y == 0, 0], 
                tsne_data[plot_y == 0, 1], c="grey", alpha=0.1, label="inlier")
    plt.scatter(tsne_data[plot_y == 1, 0], 
                tsne_data[plot_y == 1, 1], c="crimson", alpha=1, label="outlier")
    plt.legend()
    sns.despine()
    plt.title("t-SNE for outliers/inliers of " + mat_file)

    plt.subplot(1, 2, 2)
    plt.scatter(tsne_data[:, 0],
                tsne_data[:, 1],
                c=plot_loss, cmap="Reds", alpha=0.5)
    plt.colorbar()
    plt.title("Reconstruction Score on t-SNE")
    number += 1
    sns.despine()
    plt.show()

